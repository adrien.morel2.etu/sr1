package fil.coo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Client {

    private String url;
    private int port;
    private String id;
    private String password;
    private BufferedReader reader;
    private PrintWriter sender;

    public Client(String url, int port, String id, String password) {
        this.url = url;
        this.port = port;
        this.id = id;
        this.password = password;
        this.reader = reader;
        this.sender = sender;
    }

    public void connect() throws IOException {
        Socket cmdSock = new Socket(url, port);
        this.reader = new BufferedReader(new InputStreamReader(cmdSock.getInputStream()));
        this.sender = new PrintWriter(cmdSock.getOutputStream(),true);
        if(reader.readLine().startsWith("220"))
            System.out.println("Successfully connected to FTP server");
        sender.println("USER "+id);
        System.out.println(reader.readLine());
        sender.println("PASS "+password);
        if(reader.readLine().startsWith("230"))
            System.out.println("Successfully logged in FTP server");
    }

    public Socket getDataSock() throws IOException {
        this.sender.println("PASV ");
        String s = reader.readLine();
        s = s.substring(s.indexOf("(") + 1);
        s = s.substring(0, s.indexOf(")"));
        String[] tab = s.split(",");
        return new Socket(tab[0]+'.'+tab[1]+'.'+tab[2]+'.'+tab[3], Integer.parseInt(tab[4])*256+Integer.parseInt(tab[5]));
    }

    public void displayList() throws IOException {
        Socket dataSocket = getDataSock();
        BufferedReader dataRd = new BufferedReader(new InputStreamReader(dataSocket.getInputStream()));

        sender.println("LIST ");

        if(reader.readLine().startsWith("150"))
            System.out.println("Starting to list on data channel");
        System.out.println("wesh"+reader.readLine());
        String tmpFile = dataRd.readLine();
        sender.println("PWD ");
        System.out.println("zebi"+reader.readLine());
    }
}
