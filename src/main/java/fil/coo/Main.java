package fil.coo;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        String url = args[0];
        String id = args[1];
        String mdp = args[2];
        Client c = new Client(url,21,id,mdp);
        c.connect();
        c.displayList();
    }
}
